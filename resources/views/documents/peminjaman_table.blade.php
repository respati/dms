<table class="table table-responsive" id="documents-table">
    <thead>
    <tr>
        <th>Nama Peminjam</th>
        <th>Tanggal Peminjaman</th>
        <th>Tanggal Pengembalian</th>
        <th>Detail</th>
        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($documents as $document)
        <tr>
            <td>{!! $document->lent->peminjam !!}</td>
            <td>{!! $document->lent->tanggal_peminjaman !!}</td>
            <td>{!! $document->lent->tanggal_pengembalian !!}</td>
            <td>{!! $document->lent->detail !!}</td>

            <td>
                {!! Form::open(['route' => ['documents.destroy', $document->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('documents.show', [$document->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    {{--<a href="{!! route('documents.loaning', [$document->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon glyphicon-paperclip"></i></a>--}}
                    {{--<a href="{!! route('documents.edit', [$document->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>--}}
                    {{--{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}--}}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>