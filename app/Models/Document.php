<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Document
 * @package App\Models
 * @version November 26, 2017, 4:56 pm UTC
 *
 * @property string kind
 * @property string nature
 * @property string classification
 * @property string status
 * @property date activestart
 * @property date activestop
 * @property string number
 * @property string ids
 * @property date date
 * @property string from
 * @property string to
 * @property string about
 * @property string summary
 * @property string label
 * @property integer created_by
 */
class Document extends Model
{
    use SoftDeletes;

    public $table = 'dms_documents';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at','activestart','activestop'];

    public $fillable = [
        'kind',
        'nature',
        'classification',
        'status',
        'department',
        'activestart',
        'activestop',
        'number',
        'ids',
        'date',
        'pic',
        'from',
        'to',
        'about',
        'summary',
        'label',
        'retention',
        'created_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kind' => 'string',
        'nature' => 'string',
        'classification' => 'string',
        'status' => 'string',
        'department' => 'string',
        'number' => 'string',
        'ids' => 'string',
        'date' => 'date',
        'from' => 'string',
        'to' => 'string',
        'about' => 'string',
        'summary' => 'string',
        'label' => 'string',
        'retention' => 'string',
        'created_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getActivestartAttribute($value=null)
    {
        if (!$value) return null;
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getActivestopAttribute($value=null)
    {
        if (!$value) return null;
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getDateAttribute($value=null)
    {
        if (!$value) return null;
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getRetentionAttribute($value=null)
    {
        if (!$value) return null;
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function medias()
    {
        return $this->morphMany('App\Models\Media', 'mediaable');
    }

    public function user()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function lent() {
        return $this->hasOne(Document_Lent::class,'dms_document_id');
    }
}
