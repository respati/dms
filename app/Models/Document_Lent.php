<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document_Lent extends Model
{
    protected $table = "document_lents";

    protected $fillable = [
        'dms_document_id',
        'peminjam',
        'detail',
        'tanggal_peminjaman',
        'tanggal_pengembalian'
    ];
}
