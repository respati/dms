<?php

namespace App\Http\Controllers;

use App\Criteria\DocumentIsActiveCriteria;
use App\Criteria\DocumentLentedCriteria;
use App\Criteria\RetentionedCriteria;
use App\Criteria\SuratKeluarCriteria;
use App\Criteria\SuratMasukCriteria;
use App\Http\Requests\CreateDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Models\Document;
use App\Models\Document_Lent;
use App\Models\Media;
use App\Repositories\DocumentRepository;
use App\Http\Controllers\AppBaseController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Flash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpParser\Comment\Doc;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class DocumentController extends AppBaseController
{
    /** @var  DocumentRepository */
    private $documentRepository;

    public function __construct(DocumentRepository $documentRepo)
    {
        $this->documentRepository = $documentRepo;
    }

    /**
     * Display a listing of the Document.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->documentRepository->pushCriteria(new RequestCriteria($request));
        $documents = $this->documentRepository->all();

        return view('documents.index')
            ->with('documents', $documents);
    }

    /**
     * Display a listing of the Document.
     *
     * @param Request $request
     * @return Response
     */

    public function pencarian(Request $request)
    {
        $kind = $request['kind'];
        $nature = $request['nature'];
        $classification = $request['classification'];
        $status = $request['status'];
        $ids = $request['ids'];
        $number = $request['number'];
        $about = $request['about'];
        $from = $request['from'];
        $to = $request['to'];
        $department = $request['department'];
        $label = $request['label'];
        $summary = $request['summary'];
        $pic = $request['pic'];

        if (count($request->all())){
            $documents = Document::all();
            if ($kind!=-1)
            $documents = $documents->where('kind','=',$kind);
            if ($nature!=-1)
            $documents = $documents->where('nature','=',$nature);
            if ($classification!=-1)
            $documents = $documents->where('classification','=',$classification);
            if ($status!=-1)
            $documents = $documents->where('status','like',$status);
            if (!empty($ids))
            $documents = $documents->where('ids','like',$ids);
            if (!empty($number))
            $documents = $documents->where('number','like',$number);
            if (!empty($about))
            $documents = $documents->where('about','like',$about);
            if (!empty($from))
            $documents = $documents->where('from','like',$from);
            if (!empty($to))
            $documents = $documents->where('to','like',$to);
            if (!empty($pic))
            $documents = $documents->where('to','like',$pic);
            if (!empty($department))
            $documents = $documents->where('department','like',$department);
            if (!empty($label))
            $documents = $documents->where('label','like',$label);
            if (!empty($summary))
            $documents = $documents->where('summary','like',$summary);
            $documents = $documents->where('deleted_at','=',null);
        }
        else
            $documents = $this->documentRepository->with('medias')->with('user')->all();

        return view('documents.search')
            ->with('documents', $documents);
    }


    /**
     * Display a listing of the Document.
     *
     * @param Request $request
     * @return Response
     */

    public function surat_masuk(Request $request)
    {
        $this->documentRepository->pushCriteria(new RequestCriteria($request));
        $this->documentRepository->pushCriteria(DocumentIsActiveCriteria::class);
        $this->documentRepository->pushCriteria(SuratMasukCriteria::class);
        $documents = $this->documentRepository->with('medias')->all();

        return view('documents.index')
            ->with('documents', $documents);
    }

    public function peminjaman($id) {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        return view('documents.lent')->with('document', $document);
    }

    public function peminjaman_store(Request $request) {
        Document_Lent::create($request->all());

        return view('documents.peminjaman_index');
    }

    public function peminjaman_index() {
        $this->documentRepository->pushCriteria(DocumentLentedCriteria::class);
        $documents = $this->documentRepository->with('lent')->all();

        return view('documents.peminjaman_index')
            ->with('documents', $documents);
    }

    /**
     * Display a listing of the Document.
     *
     * @param Request $request
     * @return Response
     */

    public function surat_keluar(Request $request)
    {
        $this->documentRepository->pushCriteria(new RequestCriteria($request));
        $this->documentRepository->pushCriteria(SuratKeluarCriteria::class);
        $documents = $this->documentRepository->with('medias')->with('user')->all();

        return view('documents.index')
            ->with('documents', $documents);
    }

    /**
     * Display a listing of the Document.
     *
     * @param Request $request
     * @return Response
     */

    public function surat_kadaluarsa(Request $request)
    {
        $this->documentRepository->pushCriteria(new RequestCriteria($request));
        $this->documentRepository->pushCriteria(RetentionedCriteria::class);
        $documents = $this->documentRepository->with('medias')->with('user')->all();

        return view('documents.pemusnahan')
            ->with('documents', $documents);
    }

    public function pemusnahan($id)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        $document->label = "Musnah";
        $document->save();

        Flash::success('Proses berhasil.');

        return back();
    }

    /**
     * Show the form for creating a new Document.
     *
     * @return Response
     */
    public function create()
    {
        return view('documents.create');
    }

    /**
     * Store a newly created Document in storage.
     *
     * @param CreateDocumentRequest $request
     *
     * @return Response
     */
    public function store(CreateDocumentRequest $request)
    {
        $user = Auth::user();
        $input = new \stdClass();
        $input = $request->all();
        $input['created_by'] = $user->id;

        $document = $this->documentRepository->create($input);
        if ($request->hasFile('files')) {
            $files = $request->file('files');
            $path = "";
            foreach ($files as $file) {
                $media = new Media();
                $media->filename = $file->hashName();
                $document->medias()->save($media);
                if (!Storage::disk('public_uploads')->put($path, $file)) {
                    return false;
                }
            }
        }
        Flash::success('Document saved successfully.');

        return redirect(route('documents.index'));
    }

    /**
     * Display the specified Document.
     *
     * @param  int $id
     *
     * @return Response
     */

    public function show($id)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        return view('documents.show')->with('document', $document);
    }

    /**
     * Show the form for editing the specified Document.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        return view('documents.edit')->with('document', $document);
    }

    /**
     * Update the specified Document in storage.
     *
     * @param  int              $id
     * @param UpdateDocumentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDocumentRequest $request)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        $document = $this->documentRepository->update($request->all(), $id);

        if ($document->status == 0) {
            $document->activestart = null;
            $document->activestop = null;
            $document->save();
        }
        $files = $request->file('files');

        if($document->medias->count()) {
            foreach ($document->medias as $media) {
                Storage::disk('public_uploads')->delete($media->filename);
                $media->delete();
            }
        }
        if ($request->hasFile('files')) {
            foreach ($files as $file) {
                $media = new Media();
                $media->filename = $file->hashName();
                $document->medias()->save($media);
                if (!Storage::disk('public_uploads')->put('',$file)) {
                    return false;
                }
            }
        }

        Flash::success('Document updated successfully.');

        return redirect(route('documents.index'));
    }

    /**
     * Remove the specified Document from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        $this->documentRepository->delete($id);

        Flash::success('Document deleted successfully.');

        return redirect(route('documents.index'));
    }

    public function classification_list() {
        return Document::distinct()->pluck('classification') ;
    }
}
