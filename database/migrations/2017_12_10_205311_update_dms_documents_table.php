<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDmsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dms_documents', function (Blueprint $table) {
            $table->dropColumn('activestart','activestop','date');
        });
        Schema::table('dms_documents', function (Blueprint $table) {
            $table->timestamp('activestart')->nullable();
            $table->timestamp('activestop')->nullable();
            $table->timestamp('date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dms_documents', function (Blueprint $table) {
            $table->dropColumn('activestart','activestop','date');
        });
        Schema::table('dms_documents', function (Blueprint $table) {
            $table->date('activestart')->nullable();
            $table->date('activestop')->nullable();
            $table->date('date')->nullable();
        });
    }
}
