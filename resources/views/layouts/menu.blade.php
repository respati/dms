<li class="{{ Request::is('documents/surat-masuk') ? 'active' : '' }}">
    <a href="{!! route('surat_masuk') !!}"><i class="fa fa-edit"></i><span>Document In</span></a>
</li>

<li class="{{ Request::is('documents/surat_keluar') ? 'active' : '' }}">
    <a href="{!! route('surat_keluar') !!}"><i class="fa fa-edit"></i><span>Document Out</span></a>
</li>

<li class="{{ Request::is('documents/create') ? 'active' : '' }}">
    <a href="{!! route('documents.create') !!}"><i class="fa fa-edit"></i><span>Document Entry</span></a>
</li>

<li class="{{ Request::is('documents/pencarian') ? 'active' : '' }}">
    <a href="{!! route('pencarian') !!}"><i class="fa fa-edit"></i><span>Search Document</span></a>
</li>

<li class="{{ Request::is('documents/peminjaman') ? 'active' : '' }}">
    <a href="{!! route('documents.loan.index') !!}"><i class="fa fa-edit"></i><span>Document Loan</span></a>
</li>

<li class="{{ Request::is('documents/pemusnahan') ? 'active' : '' }}">
    <a href="{!! route('pemusnahan') !!}"><i class="fa fa-edit"></i><span>Document Destruction</span></a>
</li>

{{--<li class="{{ Request::is('documentsDTs*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('documentsDTs.index') !!}"><i class="fa fa-edit"></i><span>Documents D Ts</span></a>--}}
{{--</li>--}}
