@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Documents D T
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'documentsDTs.store']) !!}

                        @include('documents_d_ts.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
