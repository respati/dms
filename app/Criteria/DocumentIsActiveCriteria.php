<?php

namespace App\Criteria;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class DocumentIsActiveCriteria
 * @package namespace App\Criteria;
 */
class DocumentIsActiveCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function ($q) {
            $q->whereDate('activestop','>=',Carbon::today()->toDateString())
                ->orWhere('status','0');
        });
    }
}
