<?php

namespace App\Repositories;

use App\Models\Document;
use App\Models\SuratMasuk;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DocumentRepository
 * @package App\Repositories
 * @version November 26, 2017, 4:56 pm UTC
 *
 * @method Document findWithoutFail($id, $columns = ['*'])
 * @method Document find($id, $columns = ['*'])
 * @method Document first($columns = ['*'])
*/
class DocumentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kind',
        'nature',
        'classification',
        'status',
        'activestart',
        'activestop',
        'number',
        'ids',
        'date',
        'from',
        'to',
        'about',
        'summary',
        'label',
        'created_by'
    ];
    public function boot(){
        $this->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
            return Document::class;
    }

    public function medias()
    {
        return $this->model->morphMany('App\Models\Media', 'mediaable');
    }
}
