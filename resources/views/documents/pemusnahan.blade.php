@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1 class="pull-left">Dokumen</h1><br>
        <span class="pull-left">melebihi waktu retensi</span>
        {{--<h1 class="pull-right">--}}
        {{--<a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('documents.create') !!}">Add New</a>--}}
        {{--</h1>--}}
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-responsive" id="documents-table">
                    <thead>
                    <tr>
                        <th>Jenis</th>
                        <th>Sifat</th>
                        <th>Nama Dokumen</th>
                        <th>Status</th>
                        <th>No Surat</th>
                        <th>ID Surat</th>
                        <th>Tanggal</th>
                        <th>Dari</th>
                        <th>Kepada</th>
                        <th>Mengenai</th>
                        <th>Storage</th>
                        <th>Created By</th>
                        <th>Media</th>
                        <th colspan="3">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($documents as $document)
                        <tr>
                            <td>{!! $document->kind?'Surat Keluar':'Surat Masuk' !!}</td>
                            <td>@php
                                    switch ($document->nature){
                                        case 0: echo "Umum"; break;
                                        case 1: echo "Rahasia"; break;
                                        case 2: echo "Penting"; break;
                                        default: echo "Unknown"; break;
                                    }
                                @endphp</td>
                            <td>@php
                                    switch ($document->classification){
                                        case 0: echo "Umum"; break;
                                        case 1: echo "PKWT"; break;
                                        case 2: echo "PKWTT"; break;
                                        default: echo "Unknown"; break;
                                    }
                                @endphp</td>
                            <td>{!! $document->status==0?'Abadi':'Periode' !!}</td>
                            <td>{!! $document->number !!}</td>
                            <td>{!! $document->ids !!}</td>
                            <td>{!! $document->date !!}</td>
                            <td>{!! $document->from !!}</td>
                            <td>{!! $document->to !!}</td>
                            <td>{!! $document->about !!}</td>
                            <td>{!! $document->label !!}</td>
                            <td>{!! $document->user->name !!}</td>
                            <td>
                                Jumlah File {!! $document->medias->count() !!}
                            </td>

                            <td>
                                {!! Form::open(['route' => ['documents.expunge', $document->id], 'method' => 'post']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('documents.show', [$document->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                    {{--<a href="{!! route('documents.expunge', [$document->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-scissors"></i></a>--}}
{{--                                    <a href="{!! route('documents.edit', [$document->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>--}}
                                    {!! Form::button('<i class="glyphicon glyphicon-scissors"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

