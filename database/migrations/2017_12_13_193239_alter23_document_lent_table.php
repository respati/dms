<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter23DocumentLentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_lents', function (Blueprint $table) {
            $table->dropColumn('tanggal_peminjaman');
        });

        Schema::table('document_lents', function (Blueprint $table) {
            $table->timestamp('tanggal_peminjaman')->nullable();
            $table->timestamp('tanggal_pengembalian')->nullable();
            $table->text('nama_dokumen')->nullable();
            $table->integer('jumlah_dokumen')->nullable();
            $table->integer('status_peminjaman')->nullable();
            $table->text('informasi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_lents', function (Blueprint $table) {
            $table->dropColumn('tanggal_peminjaman');
            $table->dropColumn('tanggal_pengembalian');
            $table->dropColumn('nama_dokumen');
            $table->dropColumn('jumlah_dokumen');
            $table->dropColumn('status_peminjaman');
            $table->dropColumn('informasi');
        });

        Schema::table('document_lents', function (Blueprint $table) {
            $table->date('tanggal_peminjaman');
        });
    }
}
