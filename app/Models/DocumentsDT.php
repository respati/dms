<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DocumentsDT
 * @package App\Models
 * @version November 28, 2017, 4:47 pm UTC
 *
 * @property string kind
 * @property string nature
 * @property string classification
 * @property string status
 * @property date activestart
 * @property date activestop
 * @property string number
 * @property string ids
 * @property date date
 * @property string from
 * @property string to
 * @property string about
 * @property string summary
 * @property string label
 * @property integer created_by
 * @property string department
 */
class DocumentsDT extends Model
{
    use SoftDeletes;

    public $table = 'dms_documents';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'kind',
        'nature',
        'classification',
        'status',
        'activestart',
        'activestop',
        'number',
        'ids',
        'date',
        'from',
        'to',
        'about',
        'summary',
        'label',
        'created_by',
        'department'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kind' => 'string',
        'nature' => 'string',
        'classification' => 'string',
        'status' => 'string',
        'activestart' => 'date',
        'activestop' => 'date',
        'number' => 'string',
        'ids' => 'string',
        'date' => 'date',
        'from' => 'string',
        'to' => 'string',
        'about' => 'string',
        'summary' => 'string',
        'label' => 'string',
        'created_by' => 'integer',
        'department' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
