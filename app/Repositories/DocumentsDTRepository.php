<?php

namespace App\Repositories;

use App\Models\DocumentsDT;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DocumentsDTRepository
 * @package App\Repositories
 * @version November 28, 2017, 4:47 pm UTC
 *
 * @method DocumentsDT findWithoutFail($id, $columns = ['*'])
 * @method DocumentsDT find($id, $columns = ['*'])
 * @method DocumentsDT first($columns = ['*'])
*/
class DocumentsDTRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kind',
        'nature',
        'classification',
        'status',
        'activestart',
        'activestop',
        'number',
        'ids',
        'date',
        'from',
        'to',
        'about',
        'summary',
        'label',
        'created_by',
        'department'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DocumentsDT::class;
    }
}
