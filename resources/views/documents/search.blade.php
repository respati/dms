@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Pencarian Dokumen</h1>

        {{--<h1 class="pull-right">--}}
            {{--<a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('documents.create') !!}">Add New</a>--}}
        {{--</h1>--}}
    </section>
    <div class="content">
        <div class="clearfix"></div>
        {!! Form::open(['route' => 'pencarian']) !!}
        <div class="box box-primary" style="margin-top: 2rem">
            <div class="box-body">
                <div class="form-group col-sm-4">
                    {!! Form::label('kind', 'Jenis Surat:') !!}
                    {!! Form::select('kind', ['-1'=>'Semua','0'=>'Surat Masuk','1'=>'Surat Keluar'], null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group col-sm-4">
                    {!! Form::label('nature', 'Sifat:') !!}
                    {!! Form::select('nature', [
                                                '-1'=>'Semua',
                                                '0'=>'Umum',
                                                '1'=>'Rahasia',
                                                '2'=>'Penting'
                                            ], null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group col-sm-4">
                    {!! Form::label('classification', 'Klasifikasi:') !!}
                    {!! Form::select('classification', [
                                                '-1'=>'Semua',
                                                '0'=>'Umum',
                                                '1'=>'PKWT',
                                                '2'=>'PKWTT'
                                            ], null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group col-sm-4">
                    {!! Form::label('status', 'Status Arsip:') !!}
                    {!! Form::select('status', [
                                                '-1'=>'Semua',
                                                '0'=>'Abadi',
                                                '1'=>'Periode'
                                            ], null, ['class' => 'form-control','required']) !!}
                </div>
                <!-- ActiveFrom Field -->
                <div class="form-group col-sm-4 active-status">
                    {!! Form::label('activestart', 'Aktif Mulai:') !!}
                    {!! Form::date('activestart', null, ['class' => 'form-control', "required", "disabled"]) !!}
                </div>
                <!-- ActiveUntil Field -->
                <div class="form-group col-sm-4 active-status">
                    {!! Form::label('activestop', 'Aktif Hingga:') !!}
                    {!! Form::date('activestop', null, ['class' => 'form-control' , "required", "disabled"]) !!}
                </div>
                <div class="clearfix"></div>
                <!-- Ids Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('ids', 'ID Surat:') !!}
                    {!! Form::text('ids', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Number Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('number', 'Nomor Surat:') !!}
                    {!! Form::text('number', null, ['class' => 'form-control']) !!}
                </div>
                <!-- About Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('about', 'Perihal:') !!}
                    {!! Form::text('about', null, ['class' => 'form-control']) !!}
                </div>
                <!-- From Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('from', 'Dari:') !!}
                    {!! Form::text('from', null, ['class' => 'form-control']) !!}
                </div>
                <!-- To Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('to', 'Kepada:') !!}
                    {!! Form::text('to', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Departemen Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('department', 'Departemen:') !!}
                    {!! Form::text('department', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Date Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('date', 'Tanggal Surat:') !!}
                    {!! Form::date('date', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Label Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('label', 'Storage:') !!}
                    {!! Form::text('label', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Label Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('pic', 'PIC:') !!}
                    {!! Form::text('pic', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Summary Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('summary', 'Summary:') !!}
                    {!! Form::textarea ('summary', null, ['class' => 'form-control']) !!}
                </div>
                <div class="clearfix"></div>
                <!-- Submit Field -->
                <div class="form-group col-sm-6">
                    {!! Form::submit('Filter', ['class' => 'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}

                @include('flash::message')

                <div class="clearfix"></div>
                <div class="box box-primary">
                    <div class="box-body">
                        @include('documents.table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script type="text/javascript">

        if ($('#status').val()!="1") {
            $('.active-status').hide();
            $('.active-status input').prop("disabled",true);
        }

        $('#status').on('change', function(e) {
            var value = e.target.value;
            var datestatus = $('.active-status');
            var periodedata = $('.active-status input');
            if (value != 1){
                periodedata.prop("disabled",true)
                datestatus.hide()
            } else{
                datestatus.show()
                periodedata.prop("disabled",false)
            }
        });

        $(".select2").select2({
            tags: true,
            ajax: {
                url: '{!! route('documents.classification.all') !!}',
                dataType: 'json',
                processResults: function (data) {
                    // Tranforms the top-level key of the response object from 'items' to 'results'
                    var test = 1;
                    ndata = data.map(function(a){
                        return {
                            id: test++,
                            text:a
                        }
                    })
                    console.log(ndata)
                    return {
                        results: ndata
                    };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    </script>
@endpush

