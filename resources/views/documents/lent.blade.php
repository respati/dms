@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Document
        </h1>
    </section>

    <div class="content">
        <div class="box box-primary">
            {!! Form::open(['route' => ['documents.loaning.store', $document->id], 'method' => 'post']) !!}
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    {!! Form::hidden('dms_document_id', $document->id) !!}
                    <!-- Tanggal Peminjaman Field -->
                    <div class="form-group col-sm-3">
                        {!! Form::label('tanggal_peminjaman', 'Tanggal Peminjaman:') !!}
                        {!! Form::date('tanggal_peminjaman', null, ['class' => 'form-control']) !!}
                    </div>
                    <!-- Tanggal Pengembalian Field -->
                    <div class="form-group col-sm-3">
                        {!! Form::label('tanggal_pengembalian', 'Tanggal Pengembalian:') !!}
                        {!! Form::date('tanggal_pengembalian', null, ['class' => 'form-control']) !!}
                    </div>
                    <!-- Tanggal Pengembalian Field -->
                    <div class="form-group col-sm-3">
                        {!! Form::label('peminjam', 'Nama Peminjam:') !!}
                        {!! Form::text('peminjam', null, ['class' => 'form-control']) !!}
                    </div>
                    <!-- Tanggal Pengembalian Field -->
                    <div class="form-group col-sm-3">
                        {!! Form::label('detail', 'Detail:') !!}
                        {!! Form::text('detail', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-1 col-md-offset-9">
                        {!! Form::button('Konfirmasi', ['type' => 'submit', 'class' => 'btn btn-default', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('documents.show_fields')
                    {{--                    <a href="{!! route('documents.index') !!}" class="btn btn-default">Back</a>--}}
                </div>
            </div>
        </div>
    </div>
@endsection
