<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDmsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dms_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kind');
            $table->string('nature');
            $table->string('classification');
            $table->string('status');
            $table->date('activestart')->nullable();
            $table->date('activestop')->nullable();

            $table->string('number')->nullable();
            $table->string('department')->nullable();
            $table->string('ids')->nullable();
            $table->date('date')->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->string('about')->nullable();
            $table->string('summary')->nullable();
            $table->string('label')->nullable();

            $table->unsignedInteger('created_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dms_documents');
    }
}
