<?php
$kind = ['0'=>'Surat Masuk','1'=>'Surat Keluar'];
$nature = [
    '0'=>'Umum',
    '1'=>'Rahasia',
    '2'=>'Penting'
];
$status = [
    '0'=>'Abadi',
    '1'=>'Periode'
];
?>

<!-- Number Field -->
<div class="col-md-6">

    <div class="form-group col-sm-6">
        <label>Jenis Surat</label>
        <span class="form-control">{!! $kind[$document->kind] !!}</span>
    </div>

    <div class="form-group col-sm-6">
        <label>Sifat</label>
        <span class="form-control">{!! $nature[$document->nature] !!}</span>
    </div>

    <div class="form-group col-sm-6">
        <label>Sifat</label>
        <span class="form-control">{!! $document->classification !!}</span>
    </div>

    <div class="form-group col-sm-6">
        <label>Sifat</label>
        <span class="form-control">{!! $status[$document->status] !!}</span>
    </div>

    <!-- ActiveFrom Field -->
    <div class="form-group col-sm-6 active-status">
        {!! Form::label('activestart', 'Aktif Mulai:') !!}
        {!! Form::date('activestart', null, ['class' => 'form-control', "required",]) !!}
    </div>

    <!-- ActiveUntil Field -->
    <div class="form-group col-sm-6 active-status">
        {!! Form::label('activestop', 'Aktif Hingga:') !!}
        {!! Form::date('activestop', null, ['class' => 'form-control' , "required",]) !!}
    </div>

    <!-- Ids Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('ids', 'ID Surat:') !!}
        <span class="form-control">{!! $document->ids !!}</span>
    </div>

    <!-- Number Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('number', 'Nomor Surat:') !!}
        <span class="form-control">{!! $document->number !!}</span>
    </div>

    <!-- About Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('about', 'Perihal:') !!}
        <span class="form-control">{!! $document->about !!}</span>
    </div>

    <!-- From Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('from', 'Dari:') !!}
        <span class="form-control">{!! $document->from !!}</span>
    </div>

    <!-- To Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('to', 'Kepada:') !!}
        <span class="form-control">{!! $document->to !!}</span>
    </div>

</div>

<div class="col-md-6">

    <!-- Departemen Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('department', 'Departemen:') !!}
        <span class="form-control">{!! $document->to !!}</span>
    </div>

    <!-- Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('date', 'Tanggal Surat:') !!}
        <span class="form-control">{!! $document->date !!}</span>
    </div>

    <!-- Label Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('label', 'Tempat Penyimpanan:') !!}
        <span class="form-control">{!! $document->label !!}</span>
    </div>

    <!-- Label Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('pic', 'PIC:') !!}
        <span class="form-control">{!! $document->pic !!}</span>
    </div>

    <!-- Label Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('retention', 'Retensi:') !!}
        <span class="form-control">{!! $document->retention !!}</span>
    </div>

    <!-- Summary Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('summary', 'Summary:') !!}
        <span class="form-control">{!! $document->summary !!}</span>
    </div>

</div>

<div class="col-sm-12">
    <!-- Files Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('files[]', 'Data:') !!}
        {!! Form::file ('files[]', ['id'=>'filesupload',
                                    'class' => 'form-control',
                                    'multiple'=>'multiple',
                                    'data-preview-file-type'=>'csv',
                                    'type'=>'file'])
                                    !!}
    </div>
</div>


@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<style type="text/css">
    .kv-file-remove, .file-caption-main {display: none}
    .file-footer-caption, .drag-handle-init {display: none !important;}
    .file-thumbnail-footer {height: auto !important;}
</style>
@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>
    function getFileExtension3(filename) {
        return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
    }
    function oc(a)
    {
        var o = {};
        for(var i=0;i<a.length;i++)
        {
            o[a[i]]='';
        }
        return o;
    }
    $(function() {

        @if (isset($document) && $document->medias->count()>0)
            initialPreview = []
            initialPreviewConfig = [];
                @foreach($document->medias as $media)
                    initialPreview.push('/uploads/{!! $media->filename !!}')
                    if (getFileExtension3("{{$media->filename}}")in oc(['jpg','jpeg', 'png']))
                    {initialPreviewConfig.push({ downloadUrl:'/uploads/{{$media->filename}}',url:"/alwaysTrue"})}
                    else if (getFileExtension3("{{$media->filename}}")in oc(['pdf']))
                    {initialPreviewConfig.push({type: "pdf",  downloadUrl:'/uploads/{{$media->filename}}',url:"/alwaysTrue"})}
                    else if (getFileExtension3("{{$media->filename}}")in oc(['zip','rar','7z']))
                    {initialPreviewConfig.push({type: "object", caption: "{{$media->filename}}", downloadUrl:'/uploads/{{$media->filename}}',url:"/alwaysTrue"})}
                @endforeach
        @endif

        var opt = {
                    @if (isset($document) && $document->medias->count()>0)
                    'initialPreview': initialPreview,
                'initialPreviewConfig': initialPreviewConfig,
                initialPreviewAsData: true,
                    @endif
                    'showUpload':false,
                'showDelete':false,
                'showCancel':false,
                'showClose':false,
                'hiddenThumbnailContent':true,
                'allowedFileExtensions':['jpg','png','txt','docx','doc','pdf','zip','7zip','rar'],
                previewFileIconSettings: {
                    'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                    'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                    'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                    'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                    'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                    'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                },
                'removeFromPreviewOnError':true};
        $("#filesupload").fileinput(opt);
    });

    if ($('#status').val()!="1") {
        $('.active-status').hide();
        $('.active-status input').prop("disabled",true);
    }

    $('#status').on('change', function(e) {
        var value = e.target.value;
        var datestatus = $('.active-status');
        var periodedata = $('.active-status input');
        if (value == 0){
            periodedata.prop("disabled",true)
            datestatus.hide()
        } else{
            datestatus.show()
            periodedata.prop("disabled",false)
        }
    });


</script>
@endpush