<table class="table table-responsive" id="documents-table">
    <thead>
        <tr>
            <th>Jenis</th>
            <th>Sifat</th>
            <th>Nama Dokumen</th>
            <th>Status</th>
            <th>No Surat</th>
            <th>ID Surat</th>
            <th>Tanggal</th>
            <th>Dari</th>
            <th>Kepada</th>
            <th>Mengenai</th>
            <th>Storage</th>
            <th>Created By</th>
            <th>Media</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($documents as $document)
        <tr>
            <td>{!! $document->kind?'Surat Keluar':'Surat Masuk' !!}</td>
            <td>@php
                switch ($document->nature){
                    case 0: echo "Umum"; break;
                    case 1: echo "Rahasia"; break;
                    case 2: echo "Penting"; break;
                    default: echo "Unknown"; break;
                }
            @endphp</td>
            <td>{!! $document->classification !!}</td>
            <td>{!! $document->status==0?'Abadi':'Periode' !!}</td>
            <td>{!! $document->number !!}</td>
            <td>{!! $document->ids !!}</td>
            <td>{!! $document->date !!}</td>
            <td>{!! $document->from !!}</td>
            <td>{!! $document->to !!}</td>
            <td>{!! $document->about !!}</td>
            <td>{!! $document->label !!}</td>
            <td>{!! @$document->user->name !!}</td>
            <td>
            @if ($document->medias && $document->medias->count())
                    @foreach($document->medias as $media)
                        <a target="_blank" href="/uploads/{!! $media->filename !!}">file</a>
                    @endforeach
            @endif
            </td>

            <td>
                {!! Form::open(['route' => ['documents.destroy', $document->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('documents.show', [$document->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('documents.loaning', [$document->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon glyphicon-paperclip"></i></a>
                    <a href="{!! route('documents.edit', [$document->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>