<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentsCriteria
 * @package namespace App\Criteria;
 */
class HasMediaTrait extends Model
{
    public function commentable()
    {
        return $this->morphTo();
    }
}
