<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter2DmsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dms_documents', function (Blueprint $table) {
            $table->integer('expunged')->nullable();
            $table->integer('is_lented')->nullable();
            $table->string('lent_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dms_documents', function (Blueprint $table) {
            $table->dropColumn('expunged');
            $table->dropColumn('is_lented');
            $table->dropColumn('lent_status');
        });
    }
}
