<?php

namespace App\Http\Controllers;

use App\DataTables\DocumentsDTDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDocumentsDTRequest;
use App\Http\Requests\UpdateDocumentsDTRequest;
use App\Repositories\DocumentsDTRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DocumentsDTController extends AppBaseController
{
    /** @var  DocumentsDTRepository */
    private $documentsDTRepository;

    public function __construct(DocumentsDTRepository $documentsDTRepo)
    {
        $this->documentsDTRepository = $documentsDTRepo;
    }

    /**
     * Display a listing of the DocumentsDT.
     *
     * @param DocumentsDTDataTable $documentsDTDataTable
     * @return Response
     */
    public function index(DocumentsDTDataTable $documentsDTDataTable)
    {
        return $documentsDTDataTable->render('documents_d_ts.index');
    }

    /**
     * Show the form for creating a new DocumentsDT.
     *
     * @return Response
     */
    public function create()
    {
        return view('documents_d_ts.create');
    }

    /**
     * Store a newly created DocumentsDT in storage.
     *
     * @param CreateDocumentsDTRequest $request
     *
     * @return Response
     */
    public function store(CreateDocumentsDTRequest $request)
    {
        $input = $request->all();

        $documentsDT = $this->documentsDTRepository->create($input);

        Flash::success('Documents D T saved successfully.');

        return redirect(route('documentsDTs.index'));
    }

    /**
     * Display the specified DocumentsDT.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $documentsDT = $this->documentsDTRepository->findWithoutFail($id);

        if (empty($documentsDT)) {
            Flash::error('Documents D T not found');

            return redirect(route('documentsDTs.index'));
        }

        return view('documents_d_ts.show')->with('documentsDT', $documentsDT);
    }

    /**
     * Show the form for editing the specified DocumentsDT.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $documentsDT = $this->documentsDTRepository->findWithoutFail($id);

        if (empty($documentsDT)) {
            Flash::error('Documents D T not found');

            return redirect(route('documentsDTs.index'));
        }

        return view('documents_d_ts.edit')->with('documentsDT', $documentsDT);
    }

    /**
     * Update the specified DocumentsDT in storage.
     *
     * @param  int              $id
     * @param UpdateDocumentsDTRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDocumentsDTRequest $request)
    {
        $documentsDT = $this->documentsDTRepository->findWithoutFail($id);

        if (empty($documentsDT)) {
            Flash::error('Documents D T not found');

            return redirect(route('documentsDTs.index'));
        }

        $documentsDT = $this->documentsDTRepository->update($request->all(), $id);

        Flash::success('Documents D T updated successfully.');

        return redirect(route('documentsDTs.index'));
    }

    /**
     * Remove the specified DocumentsDT from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $documentsDT = $this->documentsDTRepository->findWithoutFail($id);

        if (empty($documentsDT)) {
            Flash::error('Documents D T not found');

            return redirect(route('documentsDTs.index'));
        }

        $this->documentsDTRepository->delete($id);

        Flash::success('Documents D T deleted successfully.');

        return redirect(route('documentsDTs.index'));
    }
}
