<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $documentsDT->id !!}</p>
</div>

<!-- Kind Field -->
<div class="form-group">
    {!! Form::label('kind', 'Kind:') !!}
    <p>{!! $documentsDT->kind !!}</p>
</div>

<!-- Nature Field -->
<div class="form-group">
    {!! Form::label('nature', 'Nature:') !!}
    <p>{!! $documentsDT->nature !!}</p>
</div>

<!-- Classification Field -->
<div class="form-group">
    {!! Form::label('classification', 'Classification:') !!}
    <p>{!! $documentsDT->classification !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $documentsDT->status !!}</p>
</div>

<!-- Activestart Field -->
<div class="form-group">
    {!! Form::label('activestart', 'Activestart:') !!}
    <p>{!! $documentsDT->activestart !!}</p>
</div>

<!-- Activestop Field -->
<div class="form-group">
    {!! Form::label('activestop', 'Activestop:') !!}
    <p>{!! $documentsDT->activestop !!}</p>
</div>

<!-- Number Field -->
<div class="form-group">
    {!! Form::label('number', 'Number:') !!}
    <p>{!! $documentsDT->number !!}</p>
</div>

<!-- Ids Field -->
<div class="form-group">
    {!! Form::label('ids', 'Ids:') !!}
    <p>{!! $documentsDT->ids !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $documentsDT->date !!}</p>
</div>

<!-- From Field -->
<div class="form-group">
    {!! Form::label('from', 'From:') !!}
    <p>{!! $documentsDT->from !!}</p>
</div>

<!-- To Field -->
<div class="form-group">
    {!! Form::label('to', 'To:') !!}
    <p>{!! $documentsDT->to !!}</p>
</div>

<!-- About Field -->
<div class="form-group">
    {!! Form::label('about', 'About:') !!}
    <p>{!! $documentsDT->about !!}</p>
</div>

<!-- Summary Field -->
<div class="form-group">
    {!! Form::label('summary', 'Summary:') !!}
    <p>{!! $documentsDT->summary !!}</p>
</div>

<!-- Label Field -->
<div class="form-group">
    {!! Form::label('label', 'Label:') !!}
    <p>{!! $documentsDT->label !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $documentsDT->created_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $documentsDT->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $documentsDT->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $documentsDT->updated_at !!}</p>
</div>

<!-- Department Field -->
<div class="form-group">
    {!! Form::label('department', 'Department:') !!}
    <p>{!! $documentsDT->department !!}</p>
</div>

