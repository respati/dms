<!-- Number Field -->
<div class="col-md-6">

    <div class="form-group col-sm-6">
        {!! Form::label('kind', 'Jenis Surat:') !!}
        {!! Form::select('kind', ['0'=>'Surat Masuk','1'=>'Surat Keluar'], null, ['class' => 'form-control','required']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('nature', 'Sifat:') !!}
        {!! Form::select('nature', [
                                    '0'=>'Umum',
                                    '1'=>'Rahasia',
                                    '2'=>'Penting'
                                ], null, ['class' => 'form-control','required']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('classification', 'Nama Dokumen:') !!}
        <select class="form-control select2" name="classification">
            @if (@$document)
            <option selected>{!! $document->classification !!}</option>
            @endif
        </select>
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('status', 'Status Arsip:') !!}
        {!! Form::select('status', [
                                    '0'=>'Abadi',
                                    '1'=>'Periode'
                                ], null, ['class' => 'form-control','required']) !!}
    </div>

    <!-- ActiveFrom Field -->
    <div class="form-group col-sm-6 active-status">
        {!! Form::label('activestart', 'Aktif Mulai:') !!}
        {!! Form::date('activestart', null, ['class' => 'form-control', "required",]) !!}
    </div>

    <!-- ActiveUntil Field -->
    <div class="form-group col-sm-6 active-status">
        {!! Form::label('activestop', 'Aktif Hingga:') !!}
        {!! Form::date('activestop', null, ['class' => 'form-control' , "required",]) !!}
    </div>

    <!-- Ids Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('ids', 'ID Surat:') !!}
        {!! Form::text('ids', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Number Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('number', 'Nomor Surat:') !!}
        {!! Form::text('number', null, ['class' => 'form-control']) !!}
    </div>

    <!-- About Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('about', 'Perihal:') !!}
        {!! Form::text('about', null, ['class' => 'form-control']) !!}
    </div>

    <!-- From Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('from', 'Dari:') !!}
        {!! Form::text('from', null, ['class' => 'form-control']) !!}
    </div>

    <!-- To Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('to', 'Kepada:') !!}
        {!! Form::text('to', null, ['class' => 'form-control']) !!}
    </div>

</div>

<div class="col-md-6">

    <!-- Departemen Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('department', 'Departemen:') !!}
        {!! Form::text('department', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('date', 'Tanggal Surat:') !!}
        {!! Form::date('date', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Label Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('label', 'Tempat Penyimpanan:') !!}
        {!! Form::text('label', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Label Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('pic', 'PIC:') !!}
        {!! Form::date('pic', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Label Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('retention', 'Retensi:') !!}
        {!! Form::date('retention', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Summary Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('summary', 'Summary:') !!}
        {!! Form::textarea ('summary', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="col-sm-12">

    <!-- Files Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('files[]', 'Upload Files:') !!}
        {!! Form::file ('files[]', ['id'=>'filesupload',
                                    'class' => 'form-control',
                                    'multiple'=>'multiple',
                                    'data-preview-file-type'=>'csv',
                                    'type'=>'file'])
                                    !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('documents.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>


@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>
    function getFileExtension3(filename) {
        return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
    }
    function oc(a)
    {
        var o = {};
        for(var i=0;i<a.length;i++)
        {
            o[a[i]]='';
        }
        return o;
    }
    $(function() {

        @if (isset($document) && $document->medias->count()>0)
            initialPreview = []
            initialPreviewConfig = [];
            @foreach($document->medias as $media)
                initialPreview.push('/uploads/{!! $media->filename !!}')
                if (getFileExtension3("{{$media->filename}}")in oc(['jpg','jpeg', 'png']))
                {initialPreviewConfig.push({caption: "<a href='/uploads/{{$media->filename}}' download> Download </a>", url:"/alwaysTrue"})}
                else if (getFileExtension3("{{$media->filename}}")in oc(['pdf']))
                {initialPreviewConfig.push({type: "pdf", caption: "<a href='/uploads/{{$media->filename}}' download> Download </a>", url:"/alwaysTrue"})}
                else if (getFileExtension3("{{$media->filename}}")in oc(['zip','rar','7z']))
                {initialPreviewConfig.push({type: "object", caption: "{{$media->filename}}", url:"/alwaysTrue"})}
            @endforeach
        @endif

        var opt = {
            @if (isset($document) && $document->medias->count()>0)
            'initialPreview': initialPreview,
            'initialPreviewConfig': initialPreviewConfig,
            initialPreviewAsData: true,
            @endif
            'showUpload':false,
            'showDelete':false,
            'showCancel':false,
            'showClose':false,
            'hiddenThumbnailContent':true,
            'allowedFileExtensions':['jpg','png','txt','docx','doc','pdf','zip','7zip','rar'],
            previewFileIconSettings: {
                'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            },
        'removeFromPreviewOnError':true};
        $("#filesupload").fileinput(opt);
    });

    if ($('#status').val()!="1") {
        $('.active-status').hide();
        $('.active-status input').prop("disabled",true);
    }

    $('#status').on('change', function(e) {
        var value = e.target.value;
        var datestatus = $('.active-status');
        var periodedata = $('.active-status input');
        if (value == 0){
            periodedata.prop("disabled",true)
            datestatus.hide()
        } else{
            datestatus.show()
            periodedata.prop("disabled",false)
        }
    });

    $(".select2").select2({
        tags: true,
        ajax: {
            url: '{!! route('documents.classification.all') !!}',
            dataType: 'json',
            processResults: function (data) {
                // Tranforms the top-level key of the response object from 'items' to 'results'
                var test = 1;
                ndata = data.map(function(a){
                        return {
                            id: test++,
                            text:a
                        }
                    })
                console.log(ndata)
                return {
                    results: ndata
                };
            }
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
    });


</script>
@endpush