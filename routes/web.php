<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('alwaysTrue', function () {
    return response('', 204)
        ->header('Content-Type', 'text/plain');
});

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::match(['get', 'post'], 'documents/pencarian', 'DocumentController@pencarian')->name('pencarian');
Route::get('documents/pemusnahan', 'DocumentController@surat_kadaluarsa')->name('pemusnahan');
Route::get('documents/surat-masuk', 'DocumentController@surat_masuk')->name('surat_masuk');
Route::get('documents/surat-keluar', 'DocumentController@surat_keluar')->name('surat_keluar');
Route::get('documents/peminjaman', 'DocumentController@peminjaman_index')->name('documents.loan.index');
Route::get('documents/peminjaman/{id}', 'DocumentController@peminjaman')->name('documents.loaning');
Route::post('documents/peminjaman/{id}', 'DocumentController@peminjaman_store')->name('documents.loaning.store');
Route::get('documents/classification-list', 'DocumentController@classification_list')->name('documents.classification.all');
Route::post('documents/expunge/{id}', 'DocumentController@pemusnahan')->name('documents.expunge');
Route::resource('documents', 'DocumentController');

Route::resource('documentsDTs', 'DocumentsDTController');